<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Product;
use App\Image;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Product::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'description' => $faker->realText(200),
        'parent_product_id' => rand(1, 5)
    ];
});

$factory->define(Image::class, function (Faker $faker) use ($factory) {
    return [
        'type' => 'png',
        'product_id' => factory(Product::class)->create()->id
    ];
});
