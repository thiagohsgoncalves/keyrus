<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('products-only/{id}', 'ProductController@getProductsWithoutChildAndImages');
$router->get('products/{id}', 'ProductController@getProductsWithChildAndImages');
$router->get('child-products/{id}', 'CategoryController@getChildProducts');
$router->get('product-images/{id}', 'CategoryController@getProductImages');
