setup:
	docker-compose pull
	docker-compose build
	cp .env.example .env
	make seed
	docker-compose up -d

seed:
	docker-compose up -d mysql
	docker-compose up -d app
	docker-compose run --rm app composer install
	@echo 'aguardando alguns segundos para subir o MySQL...'
	sleep 60
	docker-compose run --rm app php artisan migrate --seed
