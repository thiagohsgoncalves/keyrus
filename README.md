# Teste Keyrus

A aplicação utiliza 2 containers:
```bash
app >> PHP 7.4 rodando em Apache 2
mysql >> Latest
```

Para subir a aplicação, acesse a raiz do projeto via terminal e execute:
```bash
make setup
```
Isso vai demorar um pouco, vá tomar um café, rs.

Feito isso, acesse: http://localhost:8080 e verás a página inicial do Lumen.

O comando 'make setup' é responsável por: 

```bash
pull das imagens do Docker
build dos containers
instalação das dependências do PHP com o Composer
create do banco de dados (e suas tabelas) utilizando Migrations
seed das tabelas com massa de dados para testes
* caso queira visualizar a estrutura das tabelas, veja os arquivos dentro de '/database/migrations'
```

Para acessar a base de fora do container:
```bash
mysql -h127.0.0.1 -P3310 -ukeyrus -p123
* essa senha nunca deveria estar aqui, mas por se tratar de um teste, fiz vista grossa, rs
```

Rotas e Testes:
```bash
GET products-only/{id}
Ex.
curl -X GET -H 'content-type: application/json' http://localhost:8080/products-only
curl -X GET -H 'content-type: application/json' http://localhost:8080/products-only/2

GET products/{id}
Ex.
curl -X GET -H 'content-type: application/json' http://localhost:8080/products
curl -X GET -H 'content-type: application/json' http://localhost:8080/products/7

GET child-products/{id}
Ex.
curl -X GET -H 'content-type: application/json' http://localhost:8080/child-products
curl -X GET -H 'content-type: application/json' http://localhost:8080/child-products/6

GET product-images/{id}
Ex.
curl -X GET -H 'content-type: application/json' http://localhost:8080/product-images
curl -X GET -H 'content-type: application/json' http://localhost:8080/product-images/1

```
