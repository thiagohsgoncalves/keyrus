<?php

namespace App\Http\Controllers;

class ProductController extends Controller
{
    public function __construct()
    {
    }

    public function getProductsWithoutChildAndImages()
    {
    }

    public function getProductsWithChildAndImages()
    {
    }

    public function getChildProducts()
    {
    }

    public function getProductImages()
    {
    }
}
